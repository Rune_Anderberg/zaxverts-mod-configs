# Zaxvert's Mod Config
This repository contains configs for mods used by Zaxvert.

## Modlist
Currently used mods are:

* [Dispersion Indicator by Chirimen](https://wgmods.net/3271/)
* [Hangar-Manager "HangMan" by goofy67](https://wgmods.net/1170/)
* [Customization-Hangar by goofy67](http://www.goofy67-wot.de/customization-hangar.html)
* [Free Camera for Hangars and Replays by goofy67](https://wgmods.net/1495/)
* [Replay Manager by WGmods](https://wgmods.net/22/)
* [Battle Assistant by reven86](https://wgmods.net/435/)
* [PMOD by P0LIR0ID](https://wgmods.net/50/)
* [Hidden parameters by RaJCeL](https://wgmods.net/3080/)
* [Server Reticle by Awfultanker](https://awfultanker.torvox.eu/servermarker.html)
* [Battle Hits by Tornado and P0LIR0ID](https://wotspeak.org/mody-world-of-tanks-wot/805-battle-hits-wot.html)
* [XVM by XVM](https://modxvm.com/en/)
* ([MoE Calculator by Spoter](https://github.com/spoter/spoter-mods))
* ([MoE Calculator by deadhat](https://github.com/deadhat/marksOnGunExtended))