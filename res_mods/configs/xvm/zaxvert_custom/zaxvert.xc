{
	"autoReloadConfig": true,
	"definition": {
		"author": "Zaxvert",
		"description": "Zaxvert's custom settings",
		"date": "11.08.2019"
	},
	"def": {
		"carouselShadow": { "enabled": true, "color": "0x000000", "alpha": 80, "blur": 2, "strength": 2, "distance": 0, "angle": 0 }
	},
	"battle": {
		"clockFormat": "H:i:s",
		"showPostmortemDogtag": false,
		"camera": {
			"enabled": true,
			"hideHint": true,
			"arcade": {
				"distRange": [2, 128]
			},
			"postmortem": {
				"distRange": [2, 256]
			},
			"strategic": {
				"distRange": [20, 100]
			},
			"sniper": {
				"zooms": [1, 2, 4, 8, 16, 32]
			},
			"noCameraLimit": {
				"enabled": true,
				"mode": "custom"
			}
		}
	},
	"battleLabels": ${"battleLabels.xc":"labels"},
	"battleResults": {
		"showTotalExperience": true,
		"showCrewExperience": true,
		"showNetIncome": true,
		"showExtendedInfo": true,
		"showTotals": true
	},
	"damageLog": {
		"log": {
			"formatHistory": "<textformat tabstops='[30,100,170,190]'><font face='mono' size='12'>{{number%3d~.}}</font><tab><font color='{{c:dmg-kind}}'>{{dmg-kind}}</font><tab><font color='{{c:vtype}}'>{{vtype}}</font><tab><font color='{{c:team-dmg}}'>{{vehicle}}</font></textformat>"
		},
		"logAlt": {
			"formatHistory": "<textformat tabstops='[30,100,170]'><font face='mono' size='12'>{{number%3d~.}}</font><tab><font color='{{c:dmg-kind}}'>{{dmg-kind}}</font><tab><font color='{{c:team-dmg}}'>{{name%.15s~..}}</font></textformat>"
		}
	},
	"hangar": {
		"enableCrewAutoReturn": true,
		"crewReturnByDefault": true,
		"enableEquipAutoReturn": true,
		"carousel": {
			"normal": {
				"width": 128,
				"height": 80,
				"gap": 5,
				"fields": {
					"tankName": { "enabled": true, "dx": -16, "dy": -12, "alpha": 100, "scale": 0.9, "textFormat": {}, "shadow": {} },
					"favorite": { "enabled": true, "dx": -40, "dy": -20, "alpha": 100 },
					"stats": { "enabled": false, "dx": 0, "dy": 0, "alpha": 100, "textFormat": {}, "shadow": {} },
					"rentInfo": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 0.8, "textFormat": {}, "shadow": {} },
					"info": { "enabled": true, "dx": -6, "dy": 0, "alpha": 100, "scale": 0.8, "textFormat": {}, "shadow": {} },
					"infoImg": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 0.8 },
					"infoBuy": { "enabled": true, "dx": -20, "dy": 0, "alpha": 100, "scale": 1, "textFormat": {}, "shadow": {} }
				},
				"extraFields": [
					{ "x": 1, "y": 1, "layer": "substrate", "width": 128, "height": 80, "bgColor": "0x0A0A0A" },
					{
						"enabled": true,
						"x": 1, "y": 28, "width": 18, "height": 18, "alpha": "{{v.tdb?|0}}",
						"src": "xvm://res/icons/carousel/damage.png"
					},
					{
						"enabled": true,
						"x": 17, "y": 28,
						"format": "<b><font face='$FieldFont' size='12' color='{{v.c_xtdb|#CFCFCF}}'>{{v.tdb%d}}</font></b>",
						"shadow": ${ "def.carouselShadow" }
					},
					{
						"enabled": true,
						"x": 1, "y": 12, "width": 23, "height": 23,
						"src": "img://gui/maps/icons/library/proficiency/class_icons_{{v.mastery}}.png"
					},
					{
						"enabled": true,
						"x": 126, "y": 17, "align": "right", "width": 13, "height": 13, "alpha": "{{v.battles?|0}}",
						"src": "xvm://res/icons/carousel/battles.png"
					},
					{
						"enabled": true,
						"x": 113, "y": 14, "align": "right",
						"format": "<b><font face='$FieldFont' size='12' color='#CFCFCF' alpha='#F0'>{{v.battles}}</font></b>",
						"shadow": ${ "def.carouselShadow" }
					},
					{
						"enabled": true,
						"x": 126, "y": 32, "align": "right", "width": 13, "height": 13, "alpha": "{{v.winrate?|0}}",
						"src": "xvm://res/icons/carousel/wins.png"
					},
					{
						"enabled": true,
						"x": 113, "y": 28, "align": "right",
						"format": "<b><font face='$FieldFont' size='12' color='{{v.c_winrate|#CFCFCF}}'>{{v.winrate%2d~%}}</font></b>",
						"shadow": ${ "def.carouselShadow" }
					}
				]
			},
			"rows": 5,
			"nations_order": ["sweden", "uk"],
			"types_order": ["heavyTank", "mediumTank", "lightTank", "AT-SPG", "SPG"],
			"sorting_criteria": ["-premium", "-level", "nation", "type"]
		},
		"pingServers": {
		  "updateInterval": 5000
		}
	},
	"hitLog": {		
		"log": {
			"x": 550,
			"y": -23,
			"formatHistory": "<textformat leading='-4' tabstops='[30,80,130,160,180]'><font face='mono' size='12'>\u00D7{{n-player}}:</font><tab><font color='{{c:dmg-kind}}'>{{dmg}}</font><tab><font color='{{c:team-dmg}}'>{{dmg-player}}</font><tab><font face='xvm' size='15' color='#FF0000'>{{alive? |{{blownup?&#x7C;|<font size='19'>&#x77;</font>}}}}</font><tab><font color='{{c:vtype}}'>{{vtype}}</font><tab><font color='{{c:team-dmg}}'>{{vehicle}}</font></textformat>"
		},
		"logAlt": {
			"formatHistory": "<textformat leading='-4' tabstops='[30,80,130,160]'><font face='mono' size='12'>\u00D7{{n-player}}:</font><tab><font color='{{c:dmg-kind}}'>{{dmg-ratio~%}}</font><tab><font color='{{c:team-dmg}}'>{{dmg-ratio-player~%}}</font><tab><font face='xvm' size='15' color='#FF0000'>{{alive? |{{blownup?&#x7C;|<font size='19'>&#x77;</font>}}}}</font><tab><font color='{{c:team-dmg}}'>{{name%.15s~..}}</font> <font alpha='#A0'>{{clan}}</font></textformat>"
		}
	},
	"login": {
		"saveLastServer": true,
		"autologin": true//,
		//"disabledServers": ["EU2"]
	},
	"markers": {
		"enabled": true,
		"ally": {
			"alive": {				
				"normal": {
					"vehicleIcon": {      
						"enabled": true,
						"x": 0,
						"y": -16
					},
					"levelIcon": {
						"enabled": false,
						"x": 3,
						"y": -65
					}
				}
			}
		},
		"enemy": {
			"alive": {
				"normal": {
					"vehicleIcon": {      
						"enabled": true,
						"x": 0,
						"y": -16
					},
					"levelIcon": {
						"enabled": false,
						"x": 3,
						"y": -65
					}
				}
			}
		}
	},
	"playersPanel": {
		"large": {
			"standardFields": ["nick", "vehicle", "frags"],
			"nickFormatLeft": "  <font color='{{c:winrate}}' alpha='{{alive?#FF|#80}}'>{{winrate%.1f~%|--%}}</font>   <font face='mono' size='{{xvm-stat?13|0}}' color='{{c:xwn8}}' alpha='{{alive?#FF|#80}}'>{{r|--}}</font>  ",
			"nickFormatRight": "  <font face='mono' size='{{xvm-stat?13|0}}' color='{{c:xwn8}}' alpha='{{alive?#FF|#80}}'>{{r|--}}</font>   <font color='{{c:winrate}}' alpha='{{alive?#FF|#80}}'>{{winrate%.1f~%|--%}}</font>  ",
			"vehicleFormatLeft": "{{vehicle}}",
			"vehicleFormatRight": "{{vehicle}}",
			"vehicleWidth": 100
		}
	},
	"statisticForm": {
		"formatLeftVehicle": "{{vehicle}}<font face='mono' size='{{xvm-stat?13|0}}'> <font color='{{c:kb}}'>{{kb%2d~k|--k}}</font> <font color='{{c:xr}}'>{{r}}</font> <font color='{{c:winrate}}'>{{winrate%.1f~%|--%}}</font></font>",
		"formatRightVehicle": "<font face='mono' size='{{xvm-stat?13|0}}'><font color='{{c:winrate}}'>{{winrate%.1f~%|--%}}</font> <font color='{{c:xr}}'>{{r}}</font> <font color='{{c:kb}}'>{{kb%2d~k|--k}}</font> </font>{{vehicle}}",
		"squadIconOffsetXLeft": -80,
		"squadIconOffsetXRight": -80,
		"nameFieldOffsetXLeft": -81,
		"nameFieldOffsetXRight": -80
	},
	"tweaks": {
		"allowMultipleWotInstances": true
	},
	"vehicleNames": {
		// China
		"china-Ch12_111_1_2_3": 		{"name": "WZ-111 4", 			"short": "WZ-111 4"},
		// Czech		
		// France
		"france-F04_B1": 				{"name": "B1 Bis", 				"short": "B1 Bis"},
		"france-F24_Lorraine155_50": 	{"name": "Lorr. 155 50 1a",		"short": "Lorr. 155 50 1a"},
		"france-F25_Lorraine155_51": 	{"name": "Lorr. 155 50 2a", 	"short": "Lorr. 155 50 2a"},
		"france-F75_Char_de_25t": 		{"name": "FAHM 25 t", 			"short": "FAHM 25 t"},
		//
		// Germany
		"germany-G39_Marder_III": 		{"name": "Marder III H", 		"short": "Marder III H"},
		//
		// Italy
		"italy-It09_P43_ter": 			{"name": "P35/43", 				"short": "P35/43"},		
		"italy-It10_P43_bis": 			{"name": "P30/43 Bis", 			"short": "P30/43 Bis"},
		"italy-It11_P43": 				{"name": "P30/43", 				"short": "P30/43"},
		//		
		// Japan
		"japan-J22_Type_95":			{"name": "Type 95 Ro-Go", 		"short": "Type 95 Ro-Go"},
		"japan-J20_Type_2605":			{"name": "O-Ro Kai", 			"short": "O-Ro Kai"},
		"japan-J25_Type_4":				{"name": "O-Ro", 				"short": "O-Ro"},		
		"japan-J27_O_I_120":            {"name": "O-I III", 			"short": "O-I III"},
		"japan-J28_O_I_100":			{"name": "O-I II", 				"short": "O-I II"},
		"japan-J08_Chi_Nu":				{"name": "Chi-Nu II", 			"short": "Chi-Nu II"},
		"japan-J26_Type_89":			{"name": "Type 89B I-Go", 		"short": "Type 89B I-Go"},
		//
		// Poland
		"poland-Pl10_40TP_Habicha":		{"name": "25TP BS PZInż", 		"short": "25TP BS PZInż"},
		"poland-Pl12_25TP_KSUST_II":	{"name": "25TP BBTBr", 			"short": "25TP BBTBr"},
		//
		// Sweden
		"sweden-S18_EMIL_1951_E1":		{"name": "Fejkmil", 			"short": "Fejkmil"},
		"sweden-S25_EMIL_51":			{"name": "EMIL", 				"short": "EMIL"},
		//
		// UK
		"uk-GB23_Centurion":            {"name": "Centurion 3", 		"short": "Centurion 3"},
		"uk-GB92_FV217":              	{"name": "FV205", 				"short": "FV205"},
		"uk-GB19_Sherman_Firefly":      {"name": "IC Hybrid Firefly", 	"short": "IC Hybrid Firefly"},
		"uk-GB100_Manticore":      		{"name": "Chimera (1955)", 		"short": "Chimera (1955)"},
		"uk-GB07_Matilda":      		{"name": "Matilda II Mk. III",  "short": "Matilda II Mk. III"},
		"uk-GB86_Centurion_Action_X":	{"name": "Centurion MT",  		"short": "Centurion MT"},
		"uk-GB93_Caernarvon_AX":		{"name": "Caernarvon MT",  		"short": "Caernarvon MT"},
		//
		// USA
		"usa-A09_T1_hvy":           	{"name": "T1E2 Heavy", 			"short": "T1E2"},
		"usa-A11_T29":            		{"name": "T29E3", 				"short": "T29E3"},
		"usa-A124_T54E2":            	{"name": "T54E2", 				"short": "T54E2"},
		//
		// USSR
		"ussr-R02_SU-85":            	{"name": "SU-85BM", 			"short": "SU-85BM"},
		"ussr-R03_BT-7":            	{"name": "BT-7M", 				"short": "BT-7M"},
		"ussr-R08_BT-2":            	{"name": "BT-5", 				"short": "BT-5"},
		"ussr-R174_BT-5":				{"name": "BT-5E Mod. 1933",		"short": "BT-5E Mod. 1933"},
		"ussr-R104_Object_430_II":      {"name": "Obj. 430 2", 			"short": "Obj. 430 2"},
		"ussr-R139_IS_M":            	{"name": "IS-2Sh", 				"short": "IS-2Sh"},
		"ussr-R157_Object_279R":        {"name": "Obj. 726", 			"short": "Obj. 726"},
		"ussr-R81_IS8":        			{"name": "T-10M", 				"short": "T-10M"},
		"ussr-R175_IS_2_screen":        {"name": "IS-2E", 				"short": "IS-2E"},
		"ussr-R154_T_34E_1943":        	{"name": "T-34E", 				"short": "T-34E"},
		"ussr-R52_Object_261":          {"name": "Obj. 261-3", 			"short": "Obj. 261-3"}
	}
}